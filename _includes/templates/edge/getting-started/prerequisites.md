{% include templates/edge/prerequisites.md %}

##### Provisioning edge on cloud

Additionally, you will need to have Hydroculture IoT **Edge** up and running and connected to the {{currentThingsBoardVersion}} server.

{% if currentThingsBoardVersion == "Hydroculture IoT Enterprise Edition" %}
To provision Hydroculture IoT **Edge** on {{currentThingsBoardVersion}} server please visit this guide [Provision Hydroculture IoT Edge on {{currentThingsBoardVersion}} server](/docs/pe/edge/provision-edge-on-server/).
{% endif %}
{% if currentThingsBoardVersion == "Hydroculture IoT Standard Edition" %}
To provision Hydroculture IoT **Edge** on {{currentThingsBoardVersion}} server please visit this guide [Provision Hydroculture IoT Edge on {{currentThingsBoardVersion}} server](/docs/edge/provision-edge-on-server/).
{% endif %}

##### Edge installation

{% include templates/edge/provision/edge-installation.md %} 

##### User Credentials and access URLs

{% if currentThingsBoardVersion == "Hydroculture IoT Enterprise Edition" %}
{% capture contenttogglespec %}
Cloud<br/><small>Connect edge to https://hydroculture-iot.com</small>%,%cloud%,%templates/edge/pe-cloud.md%br%
On-premise server<br/><small>Connect edge to on-premise instance</small>%,%on-premise%,%templates/edge/on-premise-cloud.md{% endcapture %}
{% include content-toggle.html content-toggle-id="cloudType" toggle-spec=contenttogglespec %}
{% endif %}
{% if currentThingsBoardVersion == "Hydroculture IoT Standard Edition" %}
{% capture contenttogglespec %}
Live Demo<br/><small>Connect edge to https://demo.hydroculture-iot.com</small>%,%cloud%,%templates/edge/ce-cloud.md%br%
On-premise server<br/><small>Connect edge to on-premise instance</small>%,%on-premise%,%templates/edge/on-premise-cloud.md{% endcapture %}
{% include content-toggle.html content-toggle-id="cloudType" toggle-spec=contenttogglespec %}
{% endif %}

{% include templates/edge/bind-port-changed-banner.md %} 

We are going to refer to this URL as **http://EDGE_URL** below in tutorial.