The goal of this tutorial is to demonstrate the basic usage of the most popular Hydroculture IoT Edge features. You will learn how to:

- Connect devices to Hydroculture IoT Edge;
- Provision devices from Hydroculture IoT Edge to {{currentThingsBoardVersion}} server (cloud); 
- Push data from devices to Hydroculture IoT Edge and propagate this data to {{currentThingsBoardVersion}} server (cloud);
- Build real-time end-user dashboards on cloud and provision them to edge;

We will connect and visualize data from the temperature sensor to keep it simple.
