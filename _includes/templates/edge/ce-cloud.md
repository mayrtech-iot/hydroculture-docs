Server UI will be available using the URL: [Live Demo](https://demo.hydroculture-iot.com/signup).
Please use your [Live Demo](https://demo.hydroculture-iot.com/signup) tenant credentials to log in.

{% include templates/edge/oauth2-not-supported.md %}

We are going to refer to this URL as **http://SERVER_URL** below in tutorial.

Hydroculture IoT **Edge** UI will be available using the URL: [http://localhost:8080](http://localhost:8080).
You may use your [Live Demo](https://demo.hydroculture-iot.com/signup) tenant credentials to log in.