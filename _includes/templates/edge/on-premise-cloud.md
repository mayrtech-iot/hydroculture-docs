Server UI will be available using the URL: [http://localhost:8080](http://localhost:8080).
You may use username **tenant@hydroculture.com.org** and password **tenant**.

We are going to refer to this URL as **http://SERVER_URL** below in tutorial.

Hydroculture IoT **Edge** UI will be available using the URL: [http://localhost:18080](http://localhost:18080).
You can use the same username **tenant@hydroculture.com.org** and password **tenant** to login.