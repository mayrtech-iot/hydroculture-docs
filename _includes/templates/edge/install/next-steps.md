Congratulations! You have successfully provisioned, installed and connected Hydroculture IoT **Edge** to Hydroculture IoT server.

You can continue with **Getting started guide** to get the basic knowledge of Hydroculture IoT **Edge** or you can jump directly to more advanced topics:
{% assign currentGuide = "InstallationGuides" %}
{% assign docsPrefix = "edge/" %}
{% include templates/edge/guides-banner-edge.md %}