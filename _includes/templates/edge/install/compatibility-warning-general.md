{% capture update_server_first %}
**Hydroculture IoT Edge and Hydroculture IoT Server versions compatibility rules**:
* Hydroculture IoT Edge version X.Y.Z is compatible with Hydroculture IoT Server version X.Y.Z and above.
* Hydroculture IoT Edge version X.Y.Z is **NOT** compatible with Hydroculture IoT Server versions below X.Y.Z.

**Example**: Hydroculture IoT Edge version 3.3.4.1 is compatible with Hydroculture IoT server version 3.3.4.1 and above (3.4.0, 3.4.1, ...). 
Hydroculture IoT Edge version 3.4.0 is **NOT** compatible with Hydroculture IoT server version 3.3.4.1 or below (3.3.4, 3.3.3, ...). 
Hydroculture IoT Server 3.3.4.1 or below should be upgraded to Hydroculture IoT Server 3.4.0 or above first.

**Please make sure that Hydroculture IoT Server is updated to the latest version before proceed.**
{% endcapture %}
{% include templates/warn-banner.md content=update_server_first %}