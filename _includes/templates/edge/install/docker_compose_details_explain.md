{% if docsPrefix == 'pe/edge/' %}
{% assign appPrefix = "Hydroculture IoT Enterprise" %}
{% else %}
{% assign appPrefix = "ThingsBoard" %}
{% endif %}

Where:    
- `restart: always` - automatically start Hydroculture IoT Edge in case of system reboot and restart in case of failure;
- `8080:8080` - connect local port 8080 to exposed internal HTTP port 8080;
- `1883:1883` - connect local port 1883 to exposed internal MQTT port 1883;
- `5683-5688:5683-5688/udp` - connect local UDP ports 5683-5688 to exposed internal COAP and LwM2M ports;
- `mytb-edge-data:/data` - mounts the host's dir `mytb-edge-data` to Hydroculture IoT Edge DataBase data directory;
- `mytb-edge-logs:/var/log/tb-edge` - mounts the host's dir `mytb-edge-logs` to Hydroculture IoT Edge logs directory;
- `mytb-edge-data/db:/var/lib/postgresql/data` - mounts the host's dir `mytb-edge-data/db` to Postgres data directory;
{% if docsPrefix == 'pe/edge/' %}
- `hydroculture/tb-edge-pe:{{ site.release.pe_edge_full_ver }}` - docker image;
{% else %}
- `hydroculture/tb-edge:{{ site.release.edge_full_ver }}` - docker image;
{% endif %}
- `CLOUD_ROUTING_KEY` - your edge key;
- `CLOUD_ROUTING_SECRET` - your edge secret;
- `CLOUD_RPC_HOST` - ip address of the machine with the Hydroculture IoT platform;
{% if docsPrefix == 'pe/edge/' %}
- `CLOUD_RPC_SSL_ENABLED` - enable or disable SSL connection to server from edge.
{% endif %}

{% capture cloud_rpc_host %}
Please set **CLOUD_RPC_HOST** with an IP address of the machine where {{appPrefix}} version is running:
* DO NOT use **'localhost'** - **'localhost'** is the ip address of the edge service in the docker container.
{% if docsPrefix == 'pe/edge/' %}
* Use **hydroculture-iot.com** in case you are connecting edge to [**Hydroculture IoT Cloud**](https://hydroculture-iot.com/signup).

**NOTE**: **hydroculture-iot.com** uses SSL protocol for edge communication.
Please change **CLOUD_RPC_SSL_ENABLED** to **true** as well.
{% else %}
* Use **demo.hydroculture-iot.com** if you are connecting edge to [**Hydroculture IoT Live Demo**](https://demo.hydroculture-iot.com/signup) for evaluation.
{% endif %}
* Use **X.X.X.X** IP address in case edge is connecting to the cloud instance in the same network or in the docker.

{% endcapture %}
{% include templates/info-banner.md content=cloud_rpc_host %}

{% capture local-deployment %}
If Hydroculture IoT Edge is going to be running on the same machine where **{{appPrefix}}** server (cloud) is running, you'll need to update docker compose port mapping to avoid port collision between Hydroculture IoT server and Hydroculture IoT Edge.
 
Please update next lines of `docker-compose.yml` file:
<br>**...**
<br>**ports:**
<br> - "**18080**:8080"
<br> - "**11883**:1883"
<br> - "**15683-15688**:5683-5688/udp"
<br>**...**

Please make sure ports above (18080, 11883, 15683-15688) are not used by any other application.

{% endcapture %}
{% include templates/info-banner.md content=local-deployment %}

