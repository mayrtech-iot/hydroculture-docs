
Launch windows shell (Command Prompt) as Administrator. Change directory to your Hydroculture IoT Edge installation directory.

Execute **install.bat** script to install Hydroculture IoT Edge as a Windows service.
This means it will be automatically started on system startup. 
Similar, **uninstall.bat** will remove Hydroculture IoT Edge from Windows services.
The output should be similar to this one:

  ```text
C:\Program Files (x86)\tb-edge\install.bat
Detecting Java version installed.
CurrentVersion 110
Java 11 found!
Installing Hydroculture IoT Edge...
...
Hydroculture IoT Edge installed successfully!
```
