{% if docsPrefix == 'pe/edge/' %}
{% assign cloudLink = "[**Hydroculture IoT Cloud**](https://hydroculture-iot.com/signup)" %}
{% else %}
{% assign cloudLink = "[**Hydroculture IoT Live Demo**](https://demo.hydroculture-iot.com/signup)" %}
{% endif %}

Once started, you will be able to open **Hydroculture IoT Edge UI** using the following link **http://localhost:8080**.

{% include templates/edge/bind-port-changed-banner.md %}

Please use your tenant credentials from local cloud instance or {{cloudLink}} to log in to the Hydroculture IoT Edge.