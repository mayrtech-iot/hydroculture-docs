{% capture update_server_first %}
**Please make sure that Hydroculture IoT Server version is {{serverVersion}} or above before updating Hydroculture IoT Edge to this version**.

If Hydroculture IoT Server version is below {{serverVersion}}, please follow upgrade Hydroculture IoT server [upgrade instructions](/docs/user-guide/install/{{docsPrefix}}upgrade-instructions/{{updateServerLink}}){:target="_blank"} first.
{% endcapture %}
{% include templates/warn-banner.md content=update_server_first %}