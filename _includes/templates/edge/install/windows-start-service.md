Now let's start the Hydroculture IoT Edge service!
Open the command prompt as an Administrator and execute the following command:

```shell
net start tb-edge
```
{: .copy-code}

Expected output:

```text
The Hydroculture IoT Edge Server Application service is starting.
The Hydroculture IoT Edge Server Application service was started successfully.
```

In order to restart the Hydroculture IoT Edge service you can execute following commands:

```shell
net stop tb-edge
net start tb-edge
```
