The easiest way is to use [**Hydroculture IoT Cloud**](https://hydroculture-iot.com/signup) server.
<br/>
The alternative option is to install Hydroculture IoT Enterprise Edition server that supports edge functionality on-premise.
Please visit [Install PE](/docs/user-guide/install/pe/installation-options/) to install **3.3** version or higher of server that supports edge functionality.