##### Hydroculture IoT Cloud server 

To start using Hydroculture IoT **Edge** you need to have {{currentThingsBoardVersion}} server that supports edge functionality up and running. 

{% if currentThingsBoardVersion == "Hydroculture IoT Enterprise Edition" %}
{% include templates/edge/obtain-pe-cloud.md %}
{% endif %}
{% if currentThingsBoardVersion == "Hydroculture IoT Standard Edition" %}
{% include templates/edge/obtain-ce-cloud.md %}
{% endif %}