The easiest way is to use [Live Demo](https://demo.hydroculture-iot.com/signup) server.
<br/>
The alternative option is to install Hydroculture IoT Standard Edition server that supports edge functionality on-premise.
Please visit [Install CE](/docs/user-guide/install/installation-options/) to install **3.3** version or higher of server that supports edge functionality.