{% capture local-deployment %}
Use next **Hydroculture IoT Edge UI** link **http://localhost:18080** if you updated HTTP 8080 bind port to **18080**.
{% endcapture %}
{% include templates/info-banner.md content=local-deployment %}