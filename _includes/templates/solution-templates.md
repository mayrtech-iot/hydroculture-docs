{% if docsPrefix == "pe/" %}
{% capture paas_only %}
The Solution Templates are available in [Hydroculture IoT Cloud](/products/hydroculture-cloud/). Solution templates for [Hydroculture IoT Enterprise](/products/hydroculture-enterprise/) introduced starting from version 3.3.1PE.
{% endcapture %}
{% include templates/info-banner.md content=paas_only %}
{% endif %}
