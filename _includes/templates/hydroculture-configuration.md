## Hydroculture IoT configuration

**Note** Hydroculture IoT configuration steps are necessary only in case of **local Hydroculture IoT installation**.
If you are using [**Live Demo**](https://demo.hydroculture-iot.com/) instance all entities are pre-configured for your demo account.
However, we recommend reviewing this steps because you will still need to get device access token to send requests to Hydroculture IoT.