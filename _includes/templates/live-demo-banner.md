{% capture liveDemoContent %}
We recommend to use  [**Hydroculture IoT Cloud**](https://hydroculture-iot.com/signup) - fully managed, scalable and fault-tolerant platform for your IoT applications<br>
Hydroculture IoT Cloud is for everyone who would like to use Hydroculture IoT but don’t want to host their own instance of the platform.
{% endcapture %}
{% include templates/info-banner.md title="Hydroculture IoT Cloud" content=liveDemoContent %}




