Hydroculture IoT is a server-side platform that allows you to monitor and control IoT devices.
It is free for both personal and commercial usage and you can deploy it anywhere. 
If this is your first experience with the platform we recommend to review 
[what-is-hydroculture](/docs/getting-started-guides/what-is-hydroculture/) page and [getting-started](/docs/getting-started-guides/helloworld/) guide.
