In table below described parameters to configure authorization of IoT gateway on Hydroculture IoT platform.  

|**Parameter**|**Default value**|**Description**|
|:-|:-|-
| caCert                   | **/etc/hydroculture-gateway/ca.pem**          | Path to CA file.                                                      |
| privateKey               | **/etc/hydroculture-gateway/privateKey.pem**  | Path to private key file.                                             |
| cert                     | **/etc/hydroculture-gateway/certificate.pem** | Path to certificate file.                                             |
| checkCertPeriod          | **86400**                                    | The period in seconds when the certificate will be checked            |
| certificateDaysLeft      | **3**                                        | Days until the certificate expires, when a new one will be generated  |
|---    

Security subsection in configuration file will look like this: 

```yaml
  security:
    privateKey: /etc/hydroculture-gateway/privateKey.pem
    caCert: /etc/hydroculture-gateway/ca.pem
    cert: /etc/hydroculture-gateway/certificate.pem
    checkCertPeriod: 86400
    certificateDaysLeft: 3
```
