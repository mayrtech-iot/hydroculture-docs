In table below described parameters to configure authorization on mqtt broker .  

|**Parameter**|**Default value**|**Description**|
|:-|:-|-
| caCert                   | **/etc/hydroculture-gateway/ca.pem**          | Path to CA file.                                               |
| privateKey               | **/etc/hydroculture-gateway/privateKey.pem**  | Path to private key file.                                      |
| cert                     | **/etc/hydroculture-gateway/certificate.pem** | Path to certificate file.
|---    

Security subsection in configuration file will look like this: 

```json
  "security":{
    "caCert": "/etc/hydroculture-gateway/ca.pem",
    "privateKey": "/etc/hydroculture-gateway/privateKey.pem",
    "cert": "/etc/hydroculture-gateway/certificate.pem"
  }
```
