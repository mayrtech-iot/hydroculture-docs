## Prerequisites

You will need to have access to Hydroculture IoT Enterprise Edition. 
The easiest way is to use [Hydroculture IoT Cloud](https://hydroculture-iot.com/signup) server. 
The alternative option is to install Hydroculture IoT using [Installation Guide](/docs/user-guide/install/pe/installation-options/). 