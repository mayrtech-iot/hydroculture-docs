
Execute the following command to start Hydroculture IoT:

```bash
sudo service thingsboard start
```
{: .copy-code}
 
Once started, you will be able to open Web UI using the following link:

```bash
http://localhost:8080/
```
{: .copy-code}

The following default credentials are available if you have specified *--loadDemo* during execution of the installation script:

- **System Administrator**: sysadmin@hydroculture.com.org / sysadmin
- **Tenant Administrator**: tenant@hydroculture.com.org / tenant
- **Customer User**: customer@hydroculture.com.org / customer

You can always change passwords for each account in account profile page.