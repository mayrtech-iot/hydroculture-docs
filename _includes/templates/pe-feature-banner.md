{% capture peFeatureContent %}
Only [**Enterprise Edition**](/products/hydroculture-enterprise/) supports **{{ feature }}** feature.<br>
Use [**Hydroculture IoT Cloud**](https://hydroculture-iot.com/signup) or [**install**](/docs/user-guide/install/pe/installation-options/) your own platform instance.
{% endcapture %}
{% include templates/info-banner.md title="Hydroculture IoT Enterprise Feature" content=peFeatureContent %}
