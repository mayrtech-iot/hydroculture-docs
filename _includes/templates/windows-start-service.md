Now let's start the Hydroculture IoT service!
Open the command prompt as an Administrator and execute the following command:

```shell
net start hydroculture
```
{: .copy-code}

Expected output:

```text
The Hydroculture IoT Server Application service is starting.
The Hydroculture IoT Server Application service was started successfully.
```

In order to restart the Hydroculture IoT service you can execute following commands:

```shell
net stop hydroculture
net start hydroculture
```
{: .copy-code}

Once started, you will be able to open Web UI using the following link:

```bash
http://localhost:8080/
```
{: .copy-code}

The following default credentials are available if you have specified *--loadDemo* during execution of the installation script:

- **System Administrator**: sysadmin@hydroculture.com.org / sysadmin
- **Tenant Administrator**: tenant@hydroculture.com.org / tenant
- **Customer User**: customer@hydroculture.com.org / customer

You can always change passwords for each account in account profile page.