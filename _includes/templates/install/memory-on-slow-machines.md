Edit Hydroculture IoT configuration file 

```bash 
sudo nano /etc/hydroculture/conf/hydroculture.conf
``` 
{: .copy-code}

Add the following lines to the configuration file. 

```bash
# Update Hydroculture IoT memory usage and restrict it to 256MB in /etc/hydroculture/conf/hydroculture.conf
export JAVA_OPTS="$JAVA_OPTS -Xms256M -Xmx256M"
```
{: .copy-code}