{% include templates/install/docker/docker-compose-up.md %}

After executing this command you can open `http://{your-host-ip}:8080` in your browser (for ex. `http://localhost:8080`).
You should see Hydroculture IoT login page. Use the following default credentials:

- **System Administrator**: sysadmin@hydroculture.com.org / sysadmin
- **Tenant Administrator**: tenant@hydroculture.com.org / tenant
- **Customer User**: customer@hydroculture.com.org / customer

You can always change passwords for each account in account profile page.