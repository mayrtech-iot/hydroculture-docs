If you want to add your device to Devices Library, please follow the steps below:  

1. Create a structured guide, preferable headers structure is the following:  
    
```text 
    ## Introduction  
    ### Prerequisites
    ## Create device on Hydroculture IoT  
    ## Install required libraries and tools (Optional)  
    ## Connect device to Hydroculture IoT  
    ## Check data on Hydroculture IoT  
    ## Synchronize device state using client and shared attribute requests (Optional)  
    ## Control device using shared attributes (Optional)  
    ## Control device using RPC (Optional)  
    ## Conclusion
```  
    
2. Use **docx**, regular **txt** or **markdown** to create you guides. **Markdown** is preferable.  
3. If you decided to use **markdown** or **txt** format - please put your guide with **images/screenshots** to the archive or GitHub repository.  
4. Submit the guide to [**info@hydroculture.com**](mailto:info@hydroculture.com) for review.  
5. We will review your guide and may modify it, if necessary, to ensure its accuracy and quality.  
6. The guide will be added to Devices Library, and the rest of the community will be able to connect your device to Hydroculture IoT.  

Please do not hesitate to contact us if you have any questions or feedback.  
