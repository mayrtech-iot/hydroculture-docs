In order to get the state of the device from Hydroculture IoT during booting we have functionality to do this in the code.
Responsible parts of the example code:

Attribute callback:
```python
def sync_state(result, exception=None):
    global period
    if exception is not None:
        print("Exception: " + str(exception))
    else:
        period = result['shared']['blinkingPeriod']
```

Attribute request:
```python
def main():
    client = TBDeviceMqttClient("hydroculture-iot.com", 1883, "ACCESS_TOKEN")
    client.connect()
    client.request_attributes(shared_keys=['blinkingPeriod'], callback=sync_state)
    ...
```

In order to give ability to our callbacks to receive the data we have to send a request to Hydroculture IoT. This 
functionality allows us to keep the actual state after rebooting.

