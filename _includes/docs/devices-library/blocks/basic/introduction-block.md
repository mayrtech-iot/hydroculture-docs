
In this guide, we will learn how to [create device on Hydroculture IoT](#create-device-on-hydroculture), [install required libraries and tools](#install-required-libraries-and-tools).  
After this we will [modify our code and upload it to the device](#connect-device-to-hydroculture), and [check the results of our coding and check data on Hydroculture IoT using imported dashboard](#check-data-on-hydroculture).
Our device will synchronize with Hydroculture IoT using [client and shared attributes requests functionality](#synchronize-device-state-using-client-and-shared-attribute-requests).      
Of course, we will control our device using provided functionality like [shared attributes](#control-device-using-shared-attributes) or [RPC requests](#control-device-using-rpc).  

### Prerequisites

To continue with this guide we will need the following:  
{{ prerequisites }}
{% if page.docsPrefix == "pe/" or page.docsPrefix == "paas/" %}
- [Hydroculture IoT account](https://hydroculture-iot.com)
{% else %}
- [Hydroculture IoT account](https://demo.hydroculture-iot.com)
{% endif %}
