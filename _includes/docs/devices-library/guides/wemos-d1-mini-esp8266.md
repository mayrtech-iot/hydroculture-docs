
{% assign boardLedCount = 1 %}
{% assign deviceName = page.title | remove: "How to connect " | remove: "to Hydroculture IoT?" %}
{% assign arduinoBoardPath="**ESP8266** > **LOLIN(WEMOS) D1 R2 & mini**" %}
{% assign prerequisites = "
- " | append: deviceName | append: "
- [Arduino IDE](https://www.arduino.cc/en/software)"
 %}

## Introduction

![{{deviceName}}](/images/devices-library/{{page.deviceImageFileName}}){: style="float: left; max-width: 200px; max-height: 200px; margin: 0px 10px 0px 0px"}

The WEMOS D1 mini is based on a 32-bit ESP8266 microcontroller and has a WiFi module.  
The amount of flash memory of the device reaches 4 MB.  
The improved WEMOS D1 mini model has 16 MB of memory, an SMD antenna, and an output for connecting an external antenna.  
The sizes and location of contacts in both types of boards are the same.

{% include /docs/devices-library/blocks/basic/introduction-block.md %}

## Create device on Hydroculture IoT

{% include /docs/devices-library/blocks/basic/hydroculture-create-device-block.md %}

## Install required libraries and tools

{% include /docs/devices-library/blocks/microcontrollers/esp8266-arduino-library-install-block.md %}

{% include /docs/devices-library/blocks/microcontrollers/hydroculture-arduino-library-install-block.md %}

## Connect device to Hydroculture IoT 

{% include /docs/devices-library/blocks/basic/hydroculture-provide-device-access-token-block.md %}

{% include /docs/devices-library/blocks/microcontrollers/general-code-to-program-block.md %}

## Check data on Hydroculture IoT

{% include /docs/devices-library/blocks/basic/hydroculture-upload-example-dashboard.md %}

{% include /docs/devices-library/blocks/microcontrollers/hydroculture-check-example-data-block.md %}

## Synchronize device state using client and shared attribute requests

{% include /docs/devices-library/blocks/microcontrollers/hydroculture-synchronize-device-state-using-attribute-requests-block.md %}

## Control device using shared attributes

{% include /docs/devices-library/blocks/microcontrollers/hydroculture-update-shared-attributes-device-block.md %}

## Control device using RPC

{% include /docs/devices-library/blocks/microcontrollers/hydroculture-send-rpc-to-device-block.md %}

## Conclusion

{% include /docs/devices-library/blocks/basic/conclusion-block.md %}
