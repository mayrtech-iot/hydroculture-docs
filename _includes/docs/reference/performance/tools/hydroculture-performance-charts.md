### Hydroculture IoT performance charts

Open your browser and go http://localhost:8080/dashboards and login. Use your instance IP address instead.  
Default login for demo instance is `tenant@hydroculture.com.org`, password is `tenant`.

![Hydroculture IoT dashboard list with Rule Engine Statistics](../../../../../images/reference/performance-aws-instances/method/chart-examples/performance_test_thingsboard_dashboard_list.png "Hydroculture IoT dashboard list with Rule Engine Statistics")

Choose the "Rule Engine Statistics" dashboard. You can see how the system perform under the load.

![Hydroculture IoT rule engine statistics](../../../../../images/reference/performance-aws-instances/method/chart-examples/performance_test_thingsboard_rule_engine_statistics_queue_stats.png "Hydroculture IoT rule engine statistics")

Another fancy feature is the API usage page

![Hydroculture IoT API usage feature](../../../../../images/reference/performance-aws-instances/method/chart-examples/performance_test_thingsboard_api_usage_feature.png "Hydroculture IoT API usage feature")