---
layout: docwithnav-paas
assignees:
- ashvayka
title: What is Hydroculture IoT Cloud?
description: Features and advantages of Hydroculture IoT Cloud

---

The [**Hydroculture IoT Cloud**](/products/hydroculture-cloud/) is a fully managed, scalable and fault-tolerant platform for your IoT applications.
Hydroculture IoT Cloud is for everyone who would like to use [ThingsBoard](/docs/paas/getting-started-guides/what-is-hydroculture/) but don't want to host their own instance of the platform.
As a platform user, you get a [tenant account](/docs/paas/user-guide/entities-and-relations/) with certain entity and API [limits](/docs/paas/user-guide/tenant-profiles/#entity-limits)
that are defined based on the [subscription plan](/products/hydroculture-cloud/subscription/).      

With **Hydroculture IoT Cloud** you get the following benefits:

 - **Advanced features.** Platform supports all [Enterprise Edition](/products/hydroculture-enterprise/) features and advanced [domain management](/products/hydroculture-cloud/domains/).
 - **Improved time to market.** Save time on maintenance of the platform or configuration of the features.   
 - **Reduced costs.** The cost of the cluster infrastructure is shared between the users of the platform.
 - **High availability.** Hydroculture IoT Cloud uses microservices architecture and is deployed in multiple availability zones.
 - **Data durability.** Platform uses data replication and backup procedures to make sure you don't lose the data.
 
Apply for a [**free trial**](https://hydroculture-iot.com/signup) to get started!
