---
layout: common
title: Products
description: Hydroculture IoT Products
notitle: "true"
---

<h1 class="mainTitle products">Products</h1>

<div class="products-cards">
    <a href="/docs/getting-started-guides/what-is-hydroculture/" class="card com">
        <img src="/images/thingsboard-cm-icon.svg">
        <h5 class="title">Hydroculture IoT Cloud</h5>
        <p>Open source platform</p>
    </a>
    <a href="/products/hydroculture-edge/" class="card hydroculture-edge">
        <img src="/images/thingsboard-e-icon.svg">
        <h5 class="title">Hydroculture IoT Edge</h5>
        <p>Edge computing</p>
    </a>
    <a href="/docs/iot-gateway/what-is-iot-gateway/" class="card gateway">
        <img src="/images/gateway-icon.svg">
        <h5 class="title">Hydroculture IoT Gateway</h5>
        <p>Connect legacy protocols</p>
    </a>
    <a href="/products/mobile/" class="card mobile">
        <img src="/images/tb-mobile-icon.svg">
        <h5 class="title">Hydroculture IoT Mobile Application</h5>
        <p>IoT mobile product</p>
    </a>
</div>
