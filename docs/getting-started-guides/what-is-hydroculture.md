---
layout: docwithnav
assignees:
- ashvayka
title: What is Hydroculture IoT?
description: Hydroculture IoT key features and advantages for the rapid development of IoT projects and applications.
redirect_from: "/docs/user-guide/overview/"
---

{% include docs/getting-started-guides/what-is-hydroculture.md %}
