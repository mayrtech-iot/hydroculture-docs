---
layout: docwithnav-paas
assignees:
- ashvayka
title: Mail Settings
description: Hydroculture IoT platform mail settings

---

{% assign docsPrefix = "paas/" %}
{% include docs/user-guide/ui/mail-settings.md %}