---
layout: docwithnav-paas
assignees:
- ashvayka
title: MQTT over SSL
description: Launching Hydroculture IoT with secure MQTT protocol to connect your IoT devices and projects.

---

{% assign docsPrefix = "paas/" %}
{% include docs/user-guide/ssl/mqtt-over-ssl.md %}