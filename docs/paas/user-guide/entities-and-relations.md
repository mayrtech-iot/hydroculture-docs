---
layout: docwithnav-paas
assignees:
- ashvayka
title: Entities and relations
description: IoT asset management using Hydroculture IoT entities and relations feature

---

{% assign docsPrefix = "paas/" %}
{% include docs/user-guide/entities-and-relations.md %}
