---
layout: docwithnav-paas
title: OAuth 2.0 Support
description: OAuth 2.0 Support

---

The "OAuth 2 Support" feature is available for configuration on a System Administrator level. 
Hydroculture IoT Cloud users are Tenant Administrators and not able to access System Administrator settings.
We suggest hosting your own instance of [Hydroculture IoT Enterprise](/docs/user-guide/install/pe/installation-options/) if OAuth 2 is a necessary requirement for your setup.