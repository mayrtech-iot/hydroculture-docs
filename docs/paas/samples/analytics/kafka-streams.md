---
layout: docwithnav-paas
title: IoT data analytics using Kafka, Kafka Streams and Hydroculture IoT
description: IoT device data analytics sample using Kafka, Kafka Streams and Hydroculture IoT

---

{% assign docsPrefix = "paas/" %}
{% include /docs/samples/analytics/kafka-streams.md %}
