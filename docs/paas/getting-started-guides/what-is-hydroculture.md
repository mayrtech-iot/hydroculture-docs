---
layout: docwithnav-paas
assignees:
- ashvayka
title: What is Hydroculture IoT?
description: Hydroculture IoT key features and advantages for the rapid development of IoT projects and applications.
---

{% assign docsPrefix = "paas/" %}
{% include docs/getting-started-guides/what-is-hydroculture.md %}
