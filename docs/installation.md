---
layout: docwithnav
title: Hydroculture IoT Installation
description: Hydroculture IoT installation instructions for various operation systems and cloud platforms
notitle: "true"
---

<div class="installation">
    <div class="install-header">
       <div class="install-hero">
          <div class="container">
            <div class="install-hero-content">
                <h1>Choose Hydroculture IoT product</h1>
            </div>
            <div class="cards">
                <a href="/docs/user-guide/install/installation-options/" class="card hydroculture-standard">
                    <div class="card-title">
                        <span class="title-text">ThingsBoard<br/>Standard Edition</span>
                    </div>
                    <div class="card-img product standard-edition-bg"></div>
                    <div class="card-description">
                        Installation options
                    </div>
                </a>
                <a href="/docs/user-guide/install/pe/installation-options/" class="card hydroculture-enterprise">
                    <div class="card-title">
                        <span class="title-text">Hydroculture IoT<br/>Enterprise Edition</span>
                    </div>
                    <div class="card-img product enterprise-edition-bg"></div>
                    <div class="card-description">  
                        Installation options
                    </div>
                </a>
                <a href="/docs/iot-gateway/installation/" class="card hydroculture-gw">
                    <div class="card-title">
                        <span class="title-text">IoT Gateway</span>
                    </div>
                    <div class="card-img product gateway-bg"></div>
                    <div class="card-description">  
                        Installation options
                    </div>
                </a>
                <a href="/docs/trendz/install/installation-options/" class="card trendz">
                    <div class="card-title">
                        <span class="title-text">Trendz Analytics</span>
                    </div>
                    <div class="card-img product trendz-bg"></div>
                    <div class="card-description">  
                        Installation options
                    </div>
                </a>
            </div>
          </div>
       </div>
    </div>
</div>

