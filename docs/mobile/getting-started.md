---
layout: docwithnav-mobile
title: Getting started with Hydroculture IoT Mobile Application
description: Hydroculture IoT Mobile Application - starting point for your IoT mobile product

---

{% include docs/mobile/getting-started.md %}


