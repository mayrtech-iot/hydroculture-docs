---
layout: docwithnav
assignees:
- ashvayka
title: Installing Hydroculture IoT on Raspberry Pi 3 Model B
description: Installing Hydroculture IoT Platform on Raspberry Pi 3 Model B

---

{% include templates/live-demo-banner.md %}

* TOC
{:toc}

This guide describes how to install Hydroculture IoT on a Raspberry Pi 3 running Raspbian Buster.

### Third-party components installation

### Step 1. Install Java 11 (OpenJDK) 

{% include templates/install/ubuntu-java-install.md %}

### Step 2. Hydroculture IoT service installation

Download installation package.

```bash
wget https://gitlab.com/mayrtech-iot/hydroculture-cloud/releases/download/{{ site.release.ce_tag }}/hydroculture-{{ site.release.ce_ver }}.deb
```
{: .copy-code}

Install Hydroculture IoT as a service

```bash
sudo dpkg -i hydroculture-{{ site.release.ce_ver }}.deb
```
{: .copy-code}

### Step 3. Configure Hydroculture IoT database

{% include templates/install/rpi-db-postgresql.md %}

### Step 4. Choose Hydroculture IoT queue service

{% include templates/install/rpi-install-queue.md %}

{% capture contenttogglespecqueue %}
In Memory <small>(built-in and default)</small>%,%inmemory%,%templates/install/queue-in-memory.md%br%
AWS SQS <small>(managed service from AWS)</small>%,%aws-sqs%,%templates/install/ubuntu-queue-aws-sqs.md%br%
Google Pub/Sub <small>(managed service from Google)</small>%,%pubsub%,%templates/install/ubuntu-queue-pub-sub.md%br%
Azure Service Bus <small>(managed service from Azure)</small>%,%service-bus%,%templates/install/ubuntu-queue-service-bus.md%br%
Confluent Cloud <small>(Event Streaming Platform based on Kafka)</small>%,%confluent-cloud%,%templates/install/ubuntu-queue-confluent-cloud.md{% endcapture %}

{% include content-toggle.html content-toggle-id="ubuntuThingsboardQueue" toggle-spec=contenttogglespecqueue %} 

### Step 5. Memory update for slow machines (1GB of RAM) 

{% include templates/install/memory-on-slow-machines.md %} 

### Step 6. Run installation script
{% include templates/run-install.md %} 


### Step 7. Start Hydroculture IoT service

{% include templates/start-service.md %}

{% capture 90-sec-ui %}
Please allow up to 240 seconds for the Web UI to start. This is applicable only for slow machines with 1-2 CPUs or 1-2 GB RAM.{% endcapture %}
{% include templates/info-banner.md content=90-sec-ui %}

### Troubleshooting

{% include templates/install/troubleshooting.md %}

## Next steps

{% assign currentGuide = "InstallationGuides" %}{% include templates/guides-banner.md %}
