---
layout: docwithnav-edge
title: Installing Hydroculture IoT Edge using Docker (Linux or Mac OS)
description: Installing Hydroculture IoT Edge using Docker (Linux or Mac OS)

---

* TOC
{:toc}

{% include templates/edge/install/compatibility-warning-general.md %}

{% assign docsPrefix = "edge/" %}

This guide will help you to install and start Hydroculture IoT Edge using Docker on Linux or Mac OS.

{% include templates/edge/install/prerequisites.md %}

#### Docker installation

- [Install Docker CE](https://docs.docker.com/engine/install/){:target="_blank"}
- [Install Docker Compose](https://docs.docker.com/compose/install/){:target="_blank"}

{% include templates/install/docker-install-note.md %}

{% include templates/edge/install/hardware-requirements.md %}

### Step 1. Running Hydroculture IoT Edge

{% include templates/edge/install/docker-images-location.md %}

Create docker compose file for Hydroculture IoT Edge service:

```text
nano docker-compose.yml
```
{: .copy-code}

Add the following lines to the yml file:

```yml
version: '3.0'
services:
  mytbedge:
    restart: always
    image: "hydroculture/tb-edge:{{ site.release.edge_full_ver }}"
    ports:
      - "8080:8080"
      - "1883:1883"
      - "5683-5688:5683-5688/udp"
    environment:
      SPRING_DATASOURCE_URL: jdbc:postgresql://postgres:5432/tb-edge
      CLOUD_ROUTING_KEY: PUT_YOUR_EDGE_KEY_HERE # e.g. 19ea7ee8-5e6d-e642-4f32-05440a529015
      CLOUD_ROUTING_SECRET: PUT_YOUR_EDGE_SECRET_HERE # e.g. bztvkvfqsye7omv9uxlp
      CLOUD_RPC_HOST: PUT_YOUR_CLOUD_IP # e.g. 192.168.1.250 or demo.hydroculture-iot.com
    volumes:
      - ~/.mytb-edge-data:/data
      - ~/.mytb-edge-logs:/var/log/tb-edge
  postgres:
    restart: always
    image: "postgres:12"
    ports:
      - "5432"
    environment:
      POSTGRES_DB: tb-edge
      POSTGRES_PASSWORD: postgres
    volumes:
      - ~/.mytb-edge-data/db:/var/lib/postgresql/data
```
{: .copy-code}

{% include templates/edge/install/docker_compose_details_explain.md %}

{% include templates/install/docker/docker-create-folders-sudo-explained.md %}

{% assign serviceName = "tbedge" %}
{% include templates/install/docker/docker-compose-up.md %}

### Step 2. Open Hydroculture IoT Edge UI

{% include templates/edge/install/open-edge-ui.md %}

### Step 3. Detaching, stop and start commands

{% assign serviceFullName = "Hydroculture IoT Edge" %}
{% include templates/install/docker/detaching-stop-start-commands.md %}

## Troubleshooting

{% include templates/edge/install/docker-troubleshooting.md %}

## Next Steps

{% include templates/edge/install/next-steps.md %}



