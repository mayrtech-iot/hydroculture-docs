---
layout: docwithnav
assignees:
- ashvayka
title: Hydroculture IoT setup using AWS infrastructure
description: Hydroculture IoT platform setup with Kubernetes in AWS EKS
redirect_from: "/docs/user-guide/install/cluster/aws-cluster-setup"
  
---

{% include docs/user-guide/install/cluster/aws-eks-setup.md %}