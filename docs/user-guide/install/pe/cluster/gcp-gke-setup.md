---
layout: docwithnav-pe
assignees:
- ashvayka
title: Hydroculture IoT setup using GKE 
description: Hydroculture IoT platform setup with Kubernetes in Google Kubernetes Engine

---

* TOC
{:toc}


Here you can find scripts for different deployment scenarios using GCP infrastructure:

- [**monolith**](/docs/user-guide/install/pe/cluster/gcp-monolith-setup/) - simplistic deployment of Hydroculture IoT monolith
- [**microservices**](/docs/user-guide/install/pe/cluster/gcp-microservices-setup/) - deployment of Hydroculture IoT microservices
