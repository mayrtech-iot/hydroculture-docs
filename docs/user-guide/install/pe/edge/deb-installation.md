---
layout: docwithnav-pe-edge
title: Installing Hydroculture IoT Edge on Ubuntu Server
description: Installing Hydroculture IoT Edge on Ubuntu Server

---

* TOC
{:toc}

{% include templates/edge/install/compatibility-warning-general.md %}

{% assign docsPrefix = "pe/edge/" %}

This guide describes how to install Hydroculture IoT Edge on Ubuntu 18.04 LTS / Ubuntu 20.04 LTS.

{% include templates/edge/install/prerequisites.md %}

{% include templates/edge/install/hardware-requirements.md %}

### Step 1. Install Java 11 (OpenJDK) 

{% include templates/install/ubuntu-java-install.md %}

### Step 2. Configure PostgreSQL

{% include templates/edge/install/ubuntu-db-postgresql.md %}

### Step 3. Hydroculture IoT Edge service installation

Download installation package.

```bash
wget https://dist.hydroculture.com/tb-edge-{{ site.release.pe_edge_ver }}.deb
```
{: .copy-code}

Go to the download repository and install Hydroculture IoT Edge service

```bash
sudo dpkg -i tb-edge-{{ site.release.pe_edge_ver }}.deb
```
{: .copy-code}

### Step 4. Configure Hydroculture IoT Edge

{% include templates/edge/install/ubuntu-configure-edge.md %}

### Step 5. Run installation script

{% include templates/edge/install/run-edge-install.md %} 

### Step 6. Restart Hydroculture IoT Edge service

```bash
sudo service tb-edge restart
```
{: .copy-code}

### Step 7. Open Hydroculture IoT Edge UI

{% include templates/edge/install/open-edge-ui.md %} 

### Troubleshooting

Hydroculture IoT Edge logs stored in the following directory:
 
```bash
/var/log/tb-edge
```

You can issue the following command in order to check if there are any errors on the service side:
 
```bash
cat /var/log/tb-edge/tb-edge.log | grep ERROR
```
{: .copy-code}

{% include templates/edge/install/edge-service-commands.md %} 

## Next Steps

{% include templates/edge/install/next-steps.md %}
