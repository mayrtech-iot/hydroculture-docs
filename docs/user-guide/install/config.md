---
layout: docwithnav
assignees:
- vparomskiy
title: Configuration properties
description: Hydroculture IoT configuration properties and environment variables

---

{% include docs/user-guide/install/config.md %}
