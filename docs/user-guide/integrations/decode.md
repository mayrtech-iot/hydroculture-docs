---
layout: docwithnav
title: Monitoring District Heating Substation facility with Hydroculture IoT using Decode DL28 communication processor
description: Monitoring District Heating Substation facility with Hydroculture IoT using Decode DL28 communication processor
hidetoc: "true"
---
{% include docs/pe/user-guide/integrations/decode.md %}
