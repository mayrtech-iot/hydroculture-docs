---
layout: docwithnav
assignees:
- ashvayka
title: Access Token based authentication
description: Hydroculture IoT Access Token based authentication.

---

{% include docs/user-guide/access-token.md %}
