---
layout: docwithnav
assignees:
- ashvayka
title: Devices
description: Hydroculture IoT Device management

---

{% include docs/user-guide/ui/devices.md %}