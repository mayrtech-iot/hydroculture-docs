---
layout: docwithnav
assignees:
- ashvayka
title: Mail Settings
description: Hydroculture IoT platform mail settings

---

{% include docs/user-guide/ui/mail-settings.md %}