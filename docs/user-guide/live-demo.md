---
layout: docwithnav
assignees:
- ashvayka
title: Live Demo Server
description: Hydroculture IoT Platform Live Demo Server

---

* TOC
{:toc}

Live Demo is a single-node server that is deployed to simplify the process of getting started with the Hydroculture IoT platform.

You can access the demo server using the following URL [**demo.hydroculture-iot.com**](https://demo.hydroculture-iot.com/signup)

You will need to populate the sign-up form and as a result, you will receive tenant administrator account for your dedicated tenant.

Once you have logged in you will receive access to all dashboards and devices that are used in Hydroculture IoT [**samples**](/docs/samples/).