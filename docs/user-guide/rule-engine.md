---
layout: docwithnav
assignees:
- ashvayka
title: Rule Engine
description: IoT devices data analytics using Hydroculture IoT Rule engine
redirect_to: "/docs/user-guide/rule-engine-2-0/re-getting-started/"
---
