---
layout: docwithnav
assignees:
- ashvayka
title: Device authentication options
description: Hydroculture IoT Device authentication options.

---

{% include docs/user-guide/device-credentials.md %}