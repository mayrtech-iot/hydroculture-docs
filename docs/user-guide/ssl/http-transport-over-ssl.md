---
layout: docwithnav
assignees:
- ashvayka
title: HTTP Transport over SSL
description: Launching Hydroculture IoT with secure HTTP protocol to connect your IoT devices and projects.

---

{% include docs/user-guide/ssl/http-transport-over-ssl.md %}