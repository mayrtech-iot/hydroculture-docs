---
layout: docwithnav
assignees:
- ashvayka
title: HTTP Access Token based authentication
description: Hydroculture IoT Access Token based authentication for HTTP transport.

---

{% include docs/user-guide/ssl/http-access-token.md %}
