---
layout: docwithnav
assignees:
- ashvayka
title: HTTP over SSL
description: Launching Hydroculture IoT with secure HTTP web interface and REST API.

---

{% include docs/user-guide/ssl/http-over-ssl.md %}