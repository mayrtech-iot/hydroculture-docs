---
layout: docwithnav
title: IoT data analytics using Kafka, Kafka Streams and Hydroculture IoT
description: IoT device data analytics sample using Kafka, Kafka Streams and Hydroculture IoT

---

{% include /docs/samples/analytics/kafka-streams.md %}