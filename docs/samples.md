---
layout: docwithnav
title: Samples
description: Hydroculture IoT Platform hardware samples and tutorials

--- 

#### Demo Account

Hydroculture IoT installation contains **[single tenant account](/docs/samples/demo-account/)** that is used in sample applications and contains a lot of pre-provisioned entities for demonstration purposes.

#### Hardware platforms

The Hydroculture IoT samples are grouped by hardware platform.

**Note** Hydroculture IoT provides hardware independent APIs.
If your device platform is not in the list, but is able to setup HTTP, CoAP or MQTT connections - it is possible to integrate it with Hydroculture IoT.
Hydroculture IoT team is working on samples applications for
Intel Edison, C.H.I.P, Tessel, Samsung Artik and Gemalto 
and will upload them to this page as soon as possible.  

 - [**Arduino**](/docs/samples/arduino/)

 - [**ESP32**](/docs/samples/esp32/)

 - [**ESP8266**](/docs/samples/esp8266/)

 - [**NodeMCU**](/docs/samples/nodemcu/)

 - [**Raspberry Pi**](/docs/samples/raspberry/)

 - [**LinkIt ONE**](/docs/samples/linkit-one/)

 - [**Nettra RTU**](/docs/samples/nettrartu+/)

 - [**Smartico**](/docs/samples/smartico/)

#### Tutorials

 - [**Sending alarms using Email Plugin**](/docs/samples/alarms/mail/)
 - [**Alarms based on sensor readings**](/docs/samples/alarms/basic-rules/)
 - [**Facilities monitoring system prototype using Hydroculture IoT**](/docs/samples/monitoring/facilities-monitoring-poc/)
 
