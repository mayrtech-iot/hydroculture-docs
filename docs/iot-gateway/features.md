---
layout: docwithnav-gw
title: Hydroculture IoT Gateway Features
description: Hydroculture IoT Gateway features 
notitle: true

---

## Features

| **Feature name**        | **Link**            | **Description**                                                                             |
|:-|:-|-
| **Remote logging**      | **[How to enable remote logging](/docs/iot-gateway/guides/how-to-enable-remote-logging/)**               | This feature can help you monitor the gateway status through the Hydroculture IoT WEB interface.                                                             |
| **Gateway RPC methods** | **[How to use gateway RPC methods](/docs/iot-gateway/guides/how-to-use-gateway-rpc-methods/)**     | This feature can help you to control and get information from the gateway through Hydroculture IoT WEB interface.                                                                   |
|---

## Next steps

 - [General configuration](/docs/iot-gateway/configuration/) - how to configure Hydroculture IoT gateway.
 - [Customization guide](/docs/iot-gateway/custom/) - how to create custom connectors and converters.
