---
layout: docwithnav-gw
title: IoT Gateway
description: Hydroculture IoT Gateway to connect existing and legacy IoT devices to the platform

---

The Hydroculture **IoT Gateway** will help you to integrate devices that are connected to legacy and third-party systems with Hydroculture IoT platform.

For example, you can extract data from devices that are connected to external **MQTT brokers**, **OPC-UA servers**, **Sigfox Backend**, **Modbus slaves** or **CAN nodes**.

<div class="doc-features row mt-4">
    <div class="col-12 col-sm-6 col-lg col-xxl-6 col-4xl mb-4">
        <a class="feature-card" href="/docs/iot-gateway/what-is-iot-gateway/">
            <img class="feature-logo" src="/images/feature-logo/gateway-logo.svg"/>
            <div class="feature-title">What is Hydroculture IoT Gateway?</div>
            <div class="feature-text">
                <ul>
                    <li>Features</li>
                    <li>Architecture</li>
                </ul>
            </div>
        </a>
    </div>
    <div class="col-12 col-sm-6 col-lg col-xxl-6 col-4xl mb-4">
        <a class="feature-card" href="/docs/iot-gateway/getting-started">
            <img class="feature-logo" src="/images/feature-logo/getting-started.svg"/>
            <div class="feature-title">Getting started</div>
            <div class="feature-text">
                Provides an overview of the gateway functionality and classical "Hello World" guide.
            </div>
        </a>
    </div>
    <div class="col-12 col-sm-6 col-lg col-xxl-6 col-4xl mb-4">
        <a class="feature-card" href="/docs/iot-gateway/installation/">
            <img class="feature-logo" src="/images/feature-logo/install.svg"/>
            <div class="feature-title">Installation</div>
            <div class="feature-text">
                Learn how to install and upgrade Hydroculture IoT Gateway.
            </div>
        </a>
    </div>
    <div class="col-12 col-sm-6 col-lg col-xxl-6 col-4xl mb-4">
        <a class="feature-card" href="/docs/iot-gateway/configuration/">
            <img class="feature-logo" src="/images/feature-logo/configuration.svg"/>
            <div class="feature-title">Configuration</div>
            <div class="feature-text">
                Configuration of the core gateway services and various extensions.
            </div>
        </a>
    </div>
</div>
