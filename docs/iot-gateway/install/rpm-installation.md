---
layout: docwithnav-gw
title: Install Hydroculture IoT Gateway as package.

---


### Prerequisites

This guide describes how to install Hydroculture IoT Gateway on CentOS or RHEL.

### Step 1. Download the installation package

Download installation package.

```bash
wget https://gitlab.com/mayrtech-iot/hydroculture-gateway/releases/download/2.0/python3-hydroculture-gateway.rpm
```
{: .copy-code}

### Step 2. Install the gateway using yum

Install Hydroculture IoT Gateway as package and run it as daemon use the following command:<br><br>

```bash
sudo yum install -y ./python3-hydroculture-gateway.rpm
```
{: .copy-code}  

### Step 3. Check gateway status 

```bash
systemctl status hydroculture-gateway
```
{: .copy-code}

You may notice some errors in the output. However, it is expected, since gateway is not configured to connect to Hydroculture IoT yet:

```text
... python3[7563]: ''2019-12-26 09:31:15' - ERROR - mqtt_connector - 181 - Default Broker connection FAIL with error 5 not authorised!'
... python3[7563]: ''2019-12-26 09:31:15' - DEBUG - mqtt_connector - 186 - "Default Broker" was disconnected.'
... python3[7563]: ''2019-12-26 09:31:16' - DEBUG - tb_client - 78 - connecting to Hydroculture IoT'
... python3[7563]: ''2019-12-26 09:31:17' - DEBUG - tb_client - 78 - connecting to Hydroculture IoT'
```

### Step 4. Configure the gateway 

Now you can go to [**configuration guide**](/docs/iot-gateway/configuration/) to configure the gateway.
