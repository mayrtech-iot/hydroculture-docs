---
layout: docwithnav-gw
title: Install Hydroculture IoT Gateway using Docker.

---

* TOC
{:toc}

This guide will help you to install and start Hydroculture IoT Gateway using Docker on Linux or Mac OS.


## Prerequisites

- [Install Docker CE](https://docs.docker.com/engine/installation/)

## Running

**Execute the following command to run this docker directly:**

```
docker run -it -v ~/.tb-gateway/logs:/hydroculture_gateway/logs -v ~/.tb-gateway/extensions:/hydroculture_gateway/extensions -v ~/.tb-gateway/config:/hydroculture_gateway/config --name tb-gateway --restart always hydroculture/tb-gateway
```
{: .copy-code}

Where: 
    
- `docker run`              - run this container
- `-it`                     - attach a terminal session with current Gateway process output
- `-v ~/.tb-gateway/config:/hydroculture_gateway/config`   - mounts the host's dir `~/.tb-gateway/config` to Gateway config  directory
- `-v ~/.tb-gateway/extensions:/hydroculture_gateway/extensions`   - mounts the host's dir `~/.tb-gateway/extensions` to Gateway extensions  directory
- `-v ~/.tb-gateway/logs:/hydroculture_gateway/logs`   - mounts the host's dir `~/.tb-gateway/logs` to Gateway logs  directory
- `--name tb-gateway`             - friendly local name of this machine
- `--restart always`        - automatically start Hydroculture IoT in case of system reboot and restart in case of failure.
- `hydroculture/tb-gateway`          - docker image

## Running (with ENV variables)

**Execute the following command to run docker container with ENV variables:**

```
docker run -it -e host=hydroculture-iot.com -e port=1883 -e accessToken=ACCESS_TOKEN -v ~/.tb-gateway/logs:/hydroculture_gateway/logs -v ~/.tb-gateway/extensions:/hydroculture_gateway/extensions -v ~/.tb-gateway/config:/hydroculture_gateway/config --name tb-gateway --restart always hydroculture/tb-gateway
```
{: .copy-code}

Available  ENV variables:

| **ENV**     | **Description**                |
|:-|-
| host        | Hydroculture IoT instance host.     |
| port        | Hydroculture IoT instance port.     |
| accessToken | Gateway access token.          |
| caCert      | Path to CA file.               |
| privateKey  | Path to private key file.      |
| cert        | Path to certificate file.      |
|--

## Detaching, stop and start commands

You can detach from session terminal with `Ctrl-p` `Ctrl-q` - the container will keep running in the background.

To reattach to the terminal (to see Gateway logs) run:

```
docker attach tb-gateway
```
{: .copy-code}

To stop the container:

```
docker stop tb-gateway
```
{: .copy-code}

To start the container:

```
docker start tb-gateway
```
{: .copy-code}

## Gateway configuration

Stop the container:

```
docker stop tb-gateway
```
{: .copy-code}

**Configure gateway to work with your instance of Hydroculture IoT, using [this guide](/docs/iot-gateway/configuration/):**

Start the container after made changes:

```
docker start tb-gateway
```
{: .copy-code}

## Upgrading

In order to update to the latest image, execute the following commands:

```
docker pull hydroculture/tb-gateway
docker stop tb-gateway
docker rm tb-gateway
docker run -it -v ~/.tb-gateway/logs:/var/log/hydroculture-gateway -v ~/.tb-gateway/extensions:/var/lib/hydroculture_gateway/extensions -v ~/.tb-gateway/config:/hydroculture-gateway/config --name tb-gateway --restart always hydroculture/tb-gateway
```
{: .copy-code}

## Build local docker image

In order to build local docker image, follow the next steps:

1. Copy **hydroculture_gateway/** folder to **docker/** folder, so the final view of the directory structure will look like:
    ```text
    /hydroculture-gateway/docker
            hydroculture_gateway/
            docker-compose.yml
            Dockerfile
            LICENSE
            setup.py
            requirements.txt
    ```
2. From project root folder execute the following command:
    ```bash
    docker build -t local-gateway docker
    ```
    {: .copy-code}
