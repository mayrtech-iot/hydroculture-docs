---
layout: docwithnav-gw
title: IoT Gateway Pip installation.

---

### Package manager installation

To install Hydroculture IoT Gateway as python module, you should follow steps below:  

**1. Install required libraries to the system with apt:**  

```bash
sudo apt install python3-dev python3-pip libglib2.0-dev 
```
{: .copy-code}

**2. Install Hydroculture IoT Gateway module with pip:**  

```bash
sudo pip3 install hydroculture-gateway
```
{: .copy-code}

**3. Download example of configs, create log folder:**  

 - Downloading configs example:  

```bash
wget https://gitlab.com/mayrtech-iot/hydroculture-gateway/releases/download/2.0/configs.tar.gz
```
{: .copy-code}

 - Make directory for configs:  
```bash
sudo mkdir /etc/hydroculture-gateway
```
{: .copy-code}

 - Make directory for logs:  
```bash
sudo mkdir /var/log/hydroculture-gateway
```
{: .copy-code}

 - Unpack configs:
```bash
sudo tar -xvzf configs.tar.gz -C /etc/hydroculture-gateway
```
{: .copy-code}


**4. Set permission to the folders:**

- For logs folder:
```bash
sudo chown YOUR_USER:YOUR_USER -R /var/log/hydroculture-gateway
```
{: .copy-code}

- For configs folder
```bash
sudo chown YOUR_USER:YOUR_USER -R /etc/hydroculture-gateway
```
Where `YOUR_USER` is a user who will run the gateway.

**5. Check installation you can with command** (You will get errors about connection, because you don't configure gateway for yourself. *For configuration please use [Configuration guide](/docs/iot-gateway/configuration/)):*

```bash
hydroculture-gateway
```
{: .copy-code}
