---
layout: docwithnav-gw
title: Hydroculture IoT Gateway Roadmap
description: Architecture of Hydroculture IoT Gateway

---

The Gateway project is currently in active development stage and you should expect following major features in next releases:

 - RPC calls to connectors though the gateway.
 - GPIO support.
