---
layout: docwithnav-edge
title: Hydroculture IoT API reference
description: Hydroculture IoT API reference and supported IoT Protocols

---
{% assign docsPrefix = "edge/" %}
{% include docs/api.md %}
