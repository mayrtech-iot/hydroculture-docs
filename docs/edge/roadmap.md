---
layout: docwithnav-edge
title: Hydroculture IoT Edge Roadmap
description: Upcoming releases

---

* TOC
{:toc}

## Hydroculture IoT Edge

### v3.5

Everything in Standard 3.5, plus:

* CRUD operations for rule chains directly on the edge
* CRUD operations for assets directly on the edge
* CRUD operations for entity views directly on the edge
* CRUD operations for dashboards directly on the edge

See active development in progress [here](https://gitlab.com/mayrtech-iot/hydroculture-edge/tree/{{ site.release.branch_major_next }}) and work on latest release bug fixes [here](https://gitlab.com/mayrtech-iot/hydroculture-edge/tree/master).

### Upcoming releases
* Edge Templates
