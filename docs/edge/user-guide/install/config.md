---
layout: docwithnav-edge
title: Configuration properties
description: Hydroculture IoT Edge configuration properties and environment variables

---

{% include docs/edge/user-guide/install/config.md %}
