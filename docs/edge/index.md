---
layout: docwithnav-edge
title: Hydroculture IoT Edge
description: Hydroculture IoT Edge to distribute data processing and analysis using edge computing

---

{% include docs/edge/index.md %}